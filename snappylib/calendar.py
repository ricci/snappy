#!/usr/bin/env python3.5

from datetime import datetime
from collections import defaultdict

def partition(seq, key):
    d = defaultdict(list)
    for x in seq:
        d[key(x)].append(x)
    return d

def all_candidates(d, n):
    if not d:
        return [], []

    keep = []
    remove = []
    for key, val in d.items():
        k,r = candidates(val,n)
        keep.extend(k)
        remove.extend(r)
    return keep, remove

def candidates(l, n):
    if n <= 0:
        # Keep none
        return [], l
    elif n >= len(l):
        # keep all
        return l, []
    else:
        indices = list(map(lambda x: int(x * len(l) / n), range(n)))
        #print("Keep {} of {}: {}".format(n,len(l),indices))
        # I'm sure there's a more pythonic way to do this
        keep = []
        discard = []
        for idx, val in enumerate(l):
            if idx in indices:
                keep.append(val)
            else:
                discard.append(val)
        return keep, discard

class Calendar:
    def __init__(self,snapshots):
        self.hour_target = 1
        self.day_target = 4
        self.week_target = 14
        self.month_target = 30
        self.year_target = 4

        times = list(map(lambda x: datetime.fromtimestamp(float(x)), sorted(snapshots.keys())))
        now = datetime.now()

        self.today_by_hour = partition([x for x in times if x.date() == now.date()], lambda x: x.hour)
        self.this_month_by_day = partition([x for x in times if x.year == now.year and x.month == now.month and x.day < now.day], lambda x: x.day)
        self.this_year_by_month = partition([x for x in times if x.year == now.year and x.month < now.month], lambda x: x.month)
        self.previous_years_by_year = partition([x for x in times if x.year < now.year], lambda x: x.year)

    def today(self):
        return sum(map(lambda x: len(x),self.today_by_hour.values()))

    def this_month(self):
        return sum(map(lambda x: len(x),self.this_month_by_day.values()))

    def this_year(self):
        return sum(map(lambda x: len(x),self.this_year_by_month.values()))

    def previous_years(self):
        return sum(map(lambda x: len(x),self.previous_years_by_year.values()))

    def prune(self):
        keep = []
        remove = []

        k,r = all_candidates(self.today_by_hour, self.hour_target)
        keep.extend(k)
        remove.extend(r)
        #print("### Hourly: Keep {}, Remove {}, K: {}".format(len(k),len(r),k))

        k,r = all_candidates(self.this_month_by_day, self.day_target)
        keep.extend(k)
        remove.extend(r)
        #print("### Daily: Keep {}, Remove {}, K: {}".format(len(k),len(r),k))

        k,r = all_candidates(self.this_year_by_month, self.month_target)
        keep.extend(k)
        remove.extend(r)
        #print("### Monthly Keep {}, Remove {}".format(len(k),len(r)))

        k,r = all_candidates(self.previous_years_by_year, self.year_target)
        keep.extend(k)
        remove.extend(r)
        #print("### Yearly: Keep {}, Remove {}".format(len(k),len(r)))

        return list(map(lambda x: str(int(x.timestamp())), keep)), list(map(lambda x: str(int(x.timestamp())), remove))
